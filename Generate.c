#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define command "./Transform"    /* Keep it under 50 characters */

int main (int argc, char *argv[]) {
    double *data, *z, wall, cpu;
    int size, i, j;
    char buffer[250], filename[100], c;
    FILE *f;

    wall = (double)time(NULL);
    cpu = clock()/(double)CLOCKS_PER_SEC;

    if (argc != 3 || sscanf(argv[1],"%d%c",&size,&c) != 1 ||
            size < 1 || size > 1000000 || strlen(argv[2]) < 1 ||
            strlen(argv[2]) > 20) {
        fprintf(stderr,"Unable to decode arguments\n");
        exit(EXIT_FAILURE);
    }
    if ((data = (double *)malloc(size*size*sizeof(double))) == NULL ||
            (z = (double *)malloc(size*sizeof(double))) == NULL) {
        perror("");
        fprintf(stderr,"Unable to allocate space\n");
        exit(EXIT_FAILURE);
    }
    sprintf(filename,"%s_c_%d",argv[2],size);
    if ((f = fopen(filename,"wb")) == NULL) {
        perror("");
        fprintf(stderr,"Unable to open file for output\n");
        exit(EXIT_FAILURE);
    }
    fwrite(&size,sizeof(int),1,f);
    for (i = 0; i < size; ++i) z[i] = (2.0*rand()/(double)RAND_MAX-1.0);
    for (i = 0; i < size; ++i)
        for (j = 0; j < size; ++j)
            data[i*size+j] += z[i]*z[j]+
                (i == j ? rand()/(double)RAND_MAX : 0.0);
    fwrite(data,sizeof(double),size*size,f);
    for (i = 0; i < size; ++i)
        for (j = 0; j < size; ++j)
            data[i*size+j] = (2.0*rand()/(double)RAND_MAX-1.0);
    fwrite(data,sizeof(double),size*size,f);
    if (ferror(f) || fclose(f)) {
        perror("");
        fprintf(stderr,"Unable to write data to file\n");
        exit(EXIT_FAILURE);
    }
    sprintf(buffer,"%s %s %s_f_%d",command,filename,argv[2],size);
    system(buffer);

    printf("Time = %.2f seconds, CPU = %.2f seconds\n",
        (double)time(NULL)-wall,clock()/(double)CLOCKS_PER_SEC-cpu);
    return EXIT_SUCCESS;
}